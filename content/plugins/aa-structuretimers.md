Title: Structure Timers II
Date: 2021-04-18
Modified: 2021-04-18
Category: Plugin
Tags: aa-structuretimers
Slug: aa-structuretimers, timers
Authors: ErikKalkoken
Repository: https://gitlab.com/ErikKalkoken/aa-structuretimers
Summary: An app for keeping track of Eve Online structure timers with Alliance Auth and Discord