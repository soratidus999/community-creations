Title: AA Killtracker
Date: 2021-04-18
Modified: 2021-04-18
Category: Plugin
Tags: aa-killtracker, killmails, discord, zkill
Slug: aa-killtracker
Authors: ErikKalkoken
Repository: https://gitlab.com/ErikKalkoken/aa-killtracker
Summary: An app for running killmail trackers with Alliance Auth and Discord