Title: Discord Notify
Date: 2021-04-18
Modified: 2021-04-18
Category: Plugin
Tags: aa-discordnotify, Discord
Slug: aa-discordnotify
Authors: Erik Kalkoken
Repository: https://gitlab.com/ErikKalkoken/aa-discordnotify
Summary: This app automatically forwards Alliance Auth notifications to users on Discord.