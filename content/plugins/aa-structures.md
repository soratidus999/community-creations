Title: Structures
Date: 2021-04-18
Modified: 2021-04-18
Category: Plugin
Tags: aa-structures, structures
Slug: aa-structures
Authors: ErikKalkoken
Repository: https://gitlab.com/ErikKalkoken/aa-structures
Summary:  A plugin for monitoring alliance structures incl. a structure browser and structure notifications on Discord.