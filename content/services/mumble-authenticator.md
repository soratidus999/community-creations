Title: Mumble Authenticator
Date: 2021-04-18
Modified: 2021-04-18
Category: Service
Tags: mumble
Slug: mumble-authenticator
Authors: Alliance Auth
Repository: https://gitlab.com/allianceauth/mumble-authenticator
Summary: Authenticator script for the Alliance Auth Mumble integration