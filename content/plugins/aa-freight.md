Title: Discord Notify
Date: 2021-04-18
Modified: 2021-04-18
Category: Plugin
Tags: aa-freight, freight, Jump Freight
Slug: aa-freight
Authors: Erik Kalkoken
Repository: https://gitlab.com/ErikKalkoken/aa-discordnotify
Summary: A plugin for running a central freight service for an alliance.