Title: Package Monitor
Date: 2021-04-18
Modified: 2021-04-18
Category: Plugin
Tags: aa-package-monitor
Slug: aa-package-monitor, pip
Authors: ErikKalkoken
Repository: https://gitlab.com/ErikKalkoken/aa-package-monitor
Summary: An app that helps keep track of installed packages and outstanding updates for Alliance Auth.