Title: AA GDPR
Date: 2021-04-18
Modified: 2021-04-18
Category: Plugin
Tags: gdpr
Slug: aa-gdpr
Authors: Ariel Rin
Repository: https://gitlab.com/tactical-supremacy/aa-gdpr
Image: allianceauth.png
Summary: A Collection of resources to help Alliance Auth installs meet GDPR legislations.

This Repository cannot guarantee your Legal requirements but aims to reduce the technical burden on Web/System Administrators

## Current Features

Overrides Alliance Auth default resource bundles to use staticfile delivery.

Local staticfile delivery of  resources to avoid using CDNs

* Javascript
  * Less 3.12.2
  * Moment.js 2.27 <https://github.com/moment/moment>
  * jQuery 3.5.1 & 2.2.4 <https://github.com/jquery/jquery>
  * jQuery-DateTimePicker 2.5.20 <https://github.com/xdan/datetimepicker>
  * Twitter-Bootstrap 3.4.1 <https://github.com/twbs/bootstrap>
  * x-editable 1.5.1 <http://vitalets.github.io/x-editable>
  * Less 2.7.3 & 3.12.2 <http://lesscss.org/>
  * DataTables 1.10.21 <http://datatables.net/>
  * Clipboard.js 2.0.8 <https://clipboardjs.com/>
* Fonts
  * FontAwesome 5.14 <https://github.com/FortAwesome/Font-Awesome>
  * OFL Lato 16 <https://fonts.google.com/specimen/Lato>
* CSS
  * DataTables 1.10.21 <http://datatables.net/>
  * FontAwesome 5.14 <https://github.com/FortAwesome/Font-Awesome>
  * jQuery-DateTimePicker 2.5.20 <https://github.com/xdan/datetimepicker>
  * x-editable 1.5.1 <http://vitalets.github.io/x-editable>

## Planned Features

* Consent Management
* Terms of Use Management
* Data Transparency
* Right to be Forgotten Requests
