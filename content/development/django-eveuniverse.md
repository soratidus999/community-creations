Title: Eve Universe
Date: 2021-04-18
Modified: 2021-04-18
Category: Development
Tags: SDE
Slug: django-eveuniverse
Authors: Erik Kalkoken
Repository: https://gitlab.com/ErikKalkoken/django-eveuniverse
Summary: Complete set of Eve Online Universe models in Django with on-demand loading from ESI

*django-eveuniverse* is a foundation app meant to help speed up the development of Eve Online apps with Django and ESI. It provides all classic "static" Eve classes as Django models, including all relationships, ready to be used in your project. Furthermore, all Eve models have an on-demand loading mechanism for fetching new objects from ESI.

Here is an overview of the main features:

- Complete set of Eve Universe objects as Django models like regions, types or planets.
- On-demand loading mechanism that allows retrieving Eve universe objects ad-hoc from ESI
- Management commands for preloading often used sets of data like the map or ships types
- Eve models come with additional useful features, e.g. a route finder between solar systems or image URLs for types
- Special model EveEntity for quickly resolving Eve Online IDs to names
- Optional asynchronous loading of eve models and loading of all related children. (e.g. load all types for a specific group)