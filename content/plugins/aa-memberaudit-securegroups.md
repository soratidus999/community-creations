Title: Member Audit
Date: 2021-04-18
Modified: 2021-04-18
Category: Plugin
Tags: aa-memberaudit-securegroups, aa-memberaudit, allianceauth-secure-groups
Slug: aa-memberaudit-securegroups
Authors: eclipse-expeditions, rcmurphy
Repository: https://gitlab.com/eclipse-expeditions/aa-memberaudit-securegroups
Summary: Integration with Secure Groups for automatic group management based on activity, character age, assets, compliance, SP, or skill sets.