Title: Operation Calendar
Date: 2021-04-18
Modified: 2021-04-18
Category: Plugin
Tags: allianceauth-opcalendar, calendar, aa-moonmining, allianceauth-discordbot
Slug: allianceauth-opcalendar
Authors: paulipa
Repository: https://gitlab.com/paulipa/allianceauth-opcalendar
Summary: An operation calendar app for Alliance Auth to display fleet operations and other events.
