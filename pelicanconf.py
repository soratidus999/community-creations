#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'Alliance Auth'
SITENAME = 'Alliance Auth - Community Creations'
SITEURL = ''

PATH = 'content'
OUTPUT_PATH = 'public'
THEME = 'theme/community-creations-bs5'

TIMEZONE = 'Etc/UTC'

DEFAULT_LANG = 'en'


# Quick Modifications of Sites
DISPLAY_CATEGORIES_ON_MENU = True
DISPLAY_MENUITEMS_ON_MENU = False
MENUITEMS = (('Discord', 'https://discord.gg/fjnHAmk'),)

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Community Creations GitLab', 'https://gitlab.com/allianceauth/community-creatons'),
         ('Alliance Auth GitLab', 'https://gitlab.com/allianceauth/allianceauth/'),)

# Social widget
SOCIAL = (('Discord', 'https://discord.gg/fjnHAmk'),)

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True