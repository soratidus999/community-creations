Title: Standings Requests
Date: 2021-04-18
Modified: 2021-04-18
Category: Plugin
Tags: standings
Slug: standingsrequests
Authors: Basraah, Erik Kalkoken
Summary: Alliance Auth compatible standings tool module for requesting alt character standings and checking API key registration.